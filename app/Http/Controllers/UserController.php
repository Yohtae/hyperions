<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $users = User::paginate();
        return \view('users.users')->withUsers($users);
    }

    public function show($id)
    {
        $user = User::find($id);
        return \view('users.user')->withUser($user);
    }
}
