<?php

namespace App\Http\Controllers\shared;

use App\Category;
use App\Http\Resources\CategoriesResource;
use Illuminate\Http\Request;

class CategoriesMasterController
{

    //GET all
    public function index()
    {
        //TODO
        //return new CategoriesResource(Category::paginate());
        return new CategoriesResource(Category::paginate());
        
    }

    //GET $id
    public function show($id)
    {
        //TODO

    }

    //POST
    public function store()
    {
        //TODO

    }
}