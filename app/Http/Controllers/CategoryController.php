<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Resources\CategoriesResource; 
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    //GET all
    public function index()
    {
        //TODO
        return \view('categories.categories')->with(
            [
                // 'categories' => new CategoriesResource(Category::paginate()),
                'categories' => Category::all(),
            ]
        );
        
    }

    //GET $id
    public function show($id)
    {
        //TODO
        return \view('categories.category')->with([
            'category' => Category::findOrFail($id),
        ]);
    }

    //POST
    public function store(Request $request)
    {
        //TODO
        $request->validate([
            'category_title' => 'required',
            'category_color' => 'required',
        ]);
        $category = new Category();
        $category->title = $request->get('category_title');
        $category->color = $request->get('category_color');
        $category->save();
        return \redirect()->back()->with('status', 'category created');
    }

}
