<?php

namespace App\Http\Controllers\Api;

use App\Category;
use App\Http\Resources\CategoriesResource; 
use App\Http\Resources\PostsResource; 
use App\Http\Controllers\shared\CategoriesMasterController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryApiController extends Controller
{

    // public $masterController;

    // public function __construct()
    // {
    //     $this->masterController = new CategoriesMasterController();
    // }

    //GET all
    public function index()
    {
        //TODO
        //return new CategoriesResource(Category::paginate());
        //return new CategoriesResource(Category::paginate());
        //return $this->masterController->index();
        $categories = Category::all();
        return new CategoriesResource($categories);
    }

    //GET $id
    public function show($id)
    {
        //TODO

    }

    public function post($id)
    {
        $categories = Category::find($id);
        $post = $categories->posts;
        return new PostsResource($post);
    }
}
