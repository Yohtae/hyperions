<?php

use App\Tag;

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth'])->group(function(){

    Route::get('categories', 'CategoryController@index')->name('categories');
    Route::get('categories/{id}', 'CategoryController@show');
    Route::post('categories', 'CategoryController@store')->name('save-category');

    Route::get('tags', 'TagController@index')->name('tags');
    Route::get('tags/{id}', 'TagController@show');
    Route::post('tags', 'TagController@store')->name('save-tag');

    Route::get('comments', 'CommentController@index')->name('comments');
    Route::get('comments/{id}', 'CommentController@show');

    Route::get('users', 'UserController@index')->name('users');
    Route::get('users/{id}', 'UserController@show');

    Route::get('posts', 'PostController@index')->name('posts');
    Route::get('posts/{id}', 'PostController@show')->name('show-post');
    Route::get('new-post', 'PostController@newPost')->name('add-post');
    Route::post('new-post', 'PostController@store')->name('save-post');

});

Route::get('/test', function(){
    
    $user = App\User::find(1);
    // return $user->posts;

    //$post = App\Post::find(160);
    //$post = App\Post::paginate(15);
    //return $post->author;
    //return $post->category;
    //return $post->images;
    //return $post->videos;
    //return $post->tags;
    //return new App\Http\Resources\PostResource($post);

    // $category = App\Category::find(1);
    // return $category->posts;

    //$comment = App\Comment::find(400);
    //return $comment->post;
    //$image = App\Image::find(412);
    //return $image->post;

    //$video = App\Video::find(345);
    //return $video->post;

    //return new App\Http\Resources\PostsResource(App\Post::paginate(5));
    //return new App\Http\Resources\CategoriesResource(App\Category::paginate(5));
    return new App\Http\Resources\CommentsResource(App\Comment::paginate(5));

});

Auth::routes(['verify' => true]);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
