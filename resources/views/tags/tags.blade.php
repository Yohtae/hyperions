@extends('layouts.app')

@section('content')

    <form action="{{ route('save-tag') }}" method="POST" class="mt-3">
        @csrf
        
        <div class="form-group row">
            <label for="tag_title" class="col-md-2 col-form-label">Tag Title</label>
            <div class="col-md-9">
                <input type="text" class="form-control" id="tag_title" placeholder="Tag title" name="tag_title" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="category_color" class="col-md-2 col-form-label"></label>
            <div class="col-md-2">
                <button type="submit" class="btn btn-primary">save new tag</button>
            </div>
        </div>

    </form>

    <div class="row">
        @foreach ($tags as $tag)
            <div class="col-md-3 mt-3 mb-2">
                <div class="card">
                    <div class="card-body">
                    <h5>{{$tag->title  }}</h5>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    

@endsection