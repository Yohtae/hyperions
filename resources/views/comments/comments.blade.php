@extends('layouts.app')

@section('content')
    
<div class="row">
    
    @foreach ($comments as $comment)
    
        <div class="col-md-6 mt-3 mb-1">

            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">{{$comment->author->first_name}} {{$comment->author->last_name}}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">{{$comment->created_at}}</h6>
                    <p class="card-text">{{$comment->content}}</p>
                    <a href="{{$comment->post->link()}}" class="card-link">Go to post</a>
                    {{-- <a href="/post/{{$comment->post_id}}" class="card-link">Another link</a> --}} <!-- so work -->
                </div>
            </div>

        </div>

    @endforeach

</div>

<div class="col-lg-12 mt-3" style="align-content: center">
    {{ $comments->links() }}
</div>


@endsection