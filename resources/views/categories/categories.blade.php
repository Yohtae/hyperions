@extends('layouts.app')

@section('content')

    
    <form action="{{ route('save-category') }}" method="POST" class="mt-3">
        @csrf
        
        <div class="form-group row">
            <label for="category_title" class="col-md-2 col-form-label">Category Title</label>
            <div class="col-md-9">
                <input type="text" class="form-control" id="category_title" placeholder="Category title" name="category_title" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="category_color" class="col-md-2 col-form-label">Category Color</label>
            <div class="col-md-9">
                <input type="color" class="form-control" id="category_color" placeholder="Category color" name="category_color" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="category_color" class="col-md-2 col-form-label"></label>
            <div class="col-md-2">
                <button type="submit" class="btn btn-primary">save new category</button>
            </div>
        </div>

    </form>
    

    <div class="row">
        @foreach($categories as $category)
            <div class="col-md-3 mt-3">
                <div class="card">
                    <div class="card-body">
                        <h5>
                            {{$category->title}}   
                            <div style="background-color: {{$category->color}}; width: 50px; height: 25px"></div>
                        </h5>
                    </div>
                </div>
            </div>
        @endforeach
    </div>



@endsection
