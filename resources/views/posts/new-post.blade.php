@extends('layouts.app')

@section('content')
    
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('save-post') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row mt-4">
                            <label for="post_title" class="col-md-2 col-form-label">Post Title</label>
                            <div class="col-md-10">
                                <input type="text" name="post_title" id="post_title" placeholder="Post title" required class="form-control">
                            </div>
                        </div>
                        <div class="form-group row mt-4">
                            <label for="post_content" class="col-md-2 col-form-label">Post Content</label>
                            <div class="col-md-10">
                                <textarea class="form-control" name="post_content" id="post_content" cols="30" rows="10" placeholder="Post content" required></textarea>
                            </div>
                        </div>
                        <div class="form-group row mt-4">
                            <label for="post_title" class="col-md-2 col-form-label">Post Category</label>
                            <div class="col-md-10">
                                <select name="post_category" id="post_category" class="form-control" required>
                                    <option value="">Select category</option>
                                    @foreach ($categories as $category)
                                        <option value="{{$category->id}}">{{$category->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row mt-4">
                            <label for="post_title" class="col-md-2 col-form-label">Post Tags</label>
                            <div class="col-md-10">
                                <select name="post_tags[]" id="post_tags" class="form-control" multiple>
                                    @foreach ($tags as $tag)
                                        <option value="{{$tag->id}}">{{$tag->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row mt-4">
                            <label for="post_title" class="col-md-2 col-form-label">Post Images</label>
                            <div class="col-md-10">
                                <input type="file" name="post_images[]" id="post_images" multiple>
                            </div>
                        </div>
                        <div class="form-group row mt-4">
                            <label for="post_title" class="col-md-2 col-form-label"></label>
                            <div class="col-md-10">
                                <button type="submit" class="btn btn-primary">Save new post</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection