@extends('layouts.app')

@section('content')
    
    <div class="row">
        
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">{{$post->title}}</h5>
                    <p>{{$post->content}}</p>
                </div>
            </div>
        </div>

        <div class="col-md-4 mb-3">
            @foreach ($images as $image)
                <img src="{{ asset($image->url) }}" alt="" class="img-thumbnail mb-3">
            @endforeach

            @foreach ($videos as $video)
                <img src="{{$video->url}}" alt="" class="img-thumbnail">
            @endforeach
        </div>

    </div>

@endsection