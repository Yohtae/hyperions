@extends('layouts.app')

@section('content')

    

    <div class="row">
        @foreach ($posts as $post)
            <div class="col-md-6 bt-3 mb-2">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">{{$post->title}}</h5>
                        <a class="btn btn-primary" href="{{ route('show-post', ['id' => $post->id]) }}">
                            Read more...
                        </a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div class="col-md-6">
        {{$posts->links()}}
    </div>
    
@endsection